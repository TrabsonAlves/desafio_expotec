# Desafio_expotec

Rafael da Silva Alves 
Sistema simples com Docker e Laravel

Necessário iniciar a imagem do Docker

Para isso descompactar o arquivo imagem.zip no mesmo diretório de name-app-laravel

Iniciar o Docker

`sudo docker build mysql nginx workspace`

`sudo docker-compose restart`

Após isso provavelmente toda a aplicação já estará funcionando, hora de iniciar o banco com 
`sudo docker-compose exec workspace php name-app-laravel/artisan migrate`

Por fim, adicione a rota principal aos hosts de busca
no arquivo, pelo menos no linux, **/etc/hosts** a linha `127.0.0.1 app-laravel.dev`

No site tera inputs que servem para inserir pessoas no banco e tabém funcionam para informções de update

Abaixo serão mostrados os rgistros salvos em banco, podendo ser deletados ou atualizados