<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function(){

	return redirect('/pessoas');
});


Route::delete('/pessoas/{pessoa_id}','AcessaUser@destroy');

Route::get('/pessoas','AcessaUser@index');

Route::post('/pessoas','AcessaUser@store');

Route::patch('/pessoas/{pessoa_id}','AcessaUser@update');

