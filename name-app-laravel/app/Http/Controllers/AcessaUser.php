<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class AcessaUser extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {}
        public function index(){
	//Retorna lista de pessoas
	$query = DB::table('users')->distinct()->get();
        	$query = $query;
	$exib = $query;
	return view('lista')->with('exib',$exib);
	
	}
public function store(Request $request){
	$arr = array(
		'name' =>  $request->post('name'),
		'cidade' => $request->post('cidade'),
		'data_nascimento' =>  $request->post('nasc'));
	//Cadastra uma pessoa
		DB::table('users')->insert($arr);
return redirect('/');


	}
public function update($pessoa_id,Request $request){
	//Atualiza uma pessoa pessoas
	$arr = array(
		'name' =>  $request->post('name'),
		'cidade' =>  $request->post('cidade'),
		'data_nascimento' =>  $request->post('nasc'));
	DB::table('users')->where('id','=',$pessoa_id)->update($arr);
return redirect('/');
    	}
public function destroy($pessoa_id){
		DB::table('users')->where('id','=',$pessoa_id)->delete();

	//Deleta uma pessoa pessoas
return redirect('/');
	}
    
}
